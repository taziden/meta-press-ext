// name	  : background.js
// author : Simon Descarpentres
// date   : 2017-06
// licence: GPLv3

/* Open a new tab, and load "index.html" into it. */
function openMetaPress() {
	console.log("injecting meta-press.es");
	browser.tabs.create({
		"url": "/index.html"
	});
}
/* Add openMyPage() as a listener to clicks on the browser action. */
browser.browserAction.onClicked.addListener(openMetaPress);
