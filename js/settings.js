// name    : settings.js
// author  : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3
//
/* globals browser CodeMirror */
(async () => {
	'use strict'
	var provided_sources = await fetch("json/sources.json")
	provided_sources = await provided_sources.text()
	var custom_src_codemirror
	document.getElementById('provided_sources').textContent = provided_sources
	/* var provided_src_codemirror = */ CodeMirror.fromTextArea(
		document.getElementById('provided_sources'), {
			mode: {name: "javascript", json: true},
			indentWithTabs: true,
			readOnly: true,
		}
	)
	browser.storage.sync.get("custom_src").then(
		load_custom_src, err => {console.error(`Loading settings: ${err}`)}
	)
	function load_custom_src(stored_data){
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_src) == 'string' &&
				stored_data.custom_src) {
			document.getElementById('custom_sources').textContent = stored_data.custom_src
			document.getElementById('reload_hint').style.display = 'inline'
		} else {
			document.getElementById('custom_sources').textContent =
				document.getElementById('default_custom_sources').textContent
		}
		custom_src_codemirror = CodeMirror.fromTextArea(
			document.getElementById('custom_sources'), {
				mode: {name: "javascript", json: true},
				indentWithTabs: true,
			}
		)
		const STATUS_CURRENT_LINE = document.getElementById('ln_nb')
		const STATUS_CURRENT_COL = document.getElementById('col_nb')
		custom_src_codemirror.on("cursorActivity", () => {
			const cursor = custom_src_codemirror.getCursor()
			STATUS_CURRENT_LINE.textContent = cursor.line + 1
			STATUS_CURRENT_COL.textContent = cursor.ch + 1
		})
	}
	document.getElementById('save_custom_sources').addEventListener("click", () => {
		custom_src_codemirror.save()
		browser.storage.sync.set({custom_src: document.getElementById('custom_sources').value})
		document.getElementById('reload_hint').style.display = 'inline'
		document.getElementById('reload_hint').style['font-weight'] = 'bold'
	})
})()
