// name   : meta-press.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3
//
/* globals List, Selectr, month_nb, browser, console */
// // crypto : Firefox 26
// fetch : Firefox 39
// includes : Firefox 43
// domParser : Working Draft (Firefox 12)
// XPathEvaluator.evaluate
import * as µ from "/js/utils.js"
(async () => {
	"use strict"
	/* * definitions * */
	var HEADLINE_PAGE_SIZE = 5
	const MAX_RES_BY_SRC = 20
	const MAX_HEADLINE_LOADING = 30
	const META_FINDINGS_PAGE_SIZE = 20
	const EXCERPT_SIZE = 300  // charaters
	const HEADLINE_TITLE_SIZE = 140  // charaters
	const userLang = navigator.language || navigator.userLanguage
	var sources_objs = {}
	var sources_keys = []
	var current_source_selection = []
	var current_source_nb = 0
	var mp_req = {
		findingList: new List("findings", {  // Definition of a result record in List.js
			item: "finding-item",
			valueNames: ["f_h1", "f_txt", "f_source", "f_dt", "f_by",
				{ name: "f_title_by", attr: "title" },
				{ name: "f_ISO_dt", attr: "datetime" },
				{ name: "f_title_dt", attr: "title" },
				{ name: "f_title_txt", attr: "title" },
				{ name: "f_url", attr: "href" },
				{ name: "f_icon", attr: "src" },
			],
			page: 10,
			pagination: { outerWindow: 2, innerWindow: 3 },
			// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 char.
		}),
		metaFindingsList: new List("meta-findings", { // List of source related info for a query
			item: "meta-findings-item",
			valueNames: [
				{ name: "mf_icon", attr: "src" },
				"mf_name",
				{ name: "mf_title", attr: "title" },
				{ name: "mf_res_nb", attr: "data-mf_res_nb" },
				"mf_locale_res_nb",
			],
			page: META_FINDINGS_PAGE_SIZE,
			pagination: true
		}),
		running_query_countdown: 0,
		final_result_displayed: 0,
		query_result_timeout: null,
		query_final_result_timeout: null,
		query_start_date: null,
	}
	document.getElementById("mp_query").value = new URL(window.location).searchParams.get('q')
	document.getElementById("mp_query").addEventListener("keypress", function(evt) {
		if (!evt) evt = window.event
		var keyCode = evt.keyCode || evt.which
		if (keyCode == 13) {  // search on pressing Enter key
			document.getElementById("mp_submit").click()
			return false  // returning false will prevent the event from bubbling up.
		}
	})
	function tick_query_duration() {
		document.getElementById("mp_running_duration").textContent =
			(new Date() - mp_req.query_start_date) / 1000
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
	}
	document.getElementById("mp_submit").addEventListener("click", () => {
		if (mp_req.running_query_countdown == 0) {	// prevent 2nd search during a running one
			document.getElementById("mp_submit").disabled = true
			document.getElementById("mp_submit").style.cursor = 'not-allowed'
			document.getElementById('mp_stop').style.display = 'inline'
			document.getElementById("waiting_anim").style.display = "block"
			mp_req.search_terms = document.getElementById('mp_query').value
			mp_req.running_query_countdown = current_source_nb
			// console.log(`Countdown refill ${current_source_nb}`);
			mp_req.query_start_date = new Date()
			mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
			clear_results()
			// start_progressbar('findings_progressbar');  // 1st results appear fast enough
			const QUERY_STR = document.getElementById("mp_query").value
			current_source_selection.forEach(async (i) => {
				var n = sources_objs[i]
				var src_query_start_date = new Date()
				try {
					// https://yashints.dev/blog/2019/08/17/js-async-await
					var raw_rep = await fetch(format_search_url(n.search_url, QUERY_STR), {
						method: n.method || "GET",
						headers: { "Content-Type": "application/x-www-form-urlencoded"},
						body: n.body ? format_search_url(n.body, QUERY_STR) : null
					})
				} catch (exc) {
					search_query_countdown()
					throw new Error(`${i}: await fetch error : ${exc}`)
				}
				if (!raw_rep.ok) {
					search_query_countdown()
					throw new Error(`${i}: fetch not ok : ${raw_rep.status}`)
				}
				var c_type = raw_rep.headers.get("content-type")
				if (c_type.includes("application/json") || (n.type && n.type == 'JSON')) {
					try {
						rep = await raw_rep.json()
					} catch (exc) {
						search_query_countdown()
						throw new Error(`${i} await raw_rep.json() error : ${exc} ${raw_rep}`)
					}
				} else {
					raw_rep = await raw_rep.text()
					c_type = µ.clean_c_type(c_type)
					// console.log(c_type)
					// if (n.extends == "RSS") c_type = "text/xml"
					var rep = µ.dom_parser.parseFromString(raw_rep, c_type)
					if (µ.isParserError(rep)) {
						if (c_type.includes("xml"))
							rep = µ.HTML_encode_UTF8(µ.HTML_decode_entities(raw_rep))
						// console.log(rep)
						rep = µ.dom_parser.parseFromString(rep, c_type)
						if (µ.isParserError(rep)) {
							search_query_countdown()
							// console.error(`${i} dom parser error`, rep)
							throw new Error(`${i} dom parser error ${rep}`, rep)
						}
					}
				}
				if (!n.domain_part) n.domain_part = µ.domain_part(i)
				if (!n.favicon_url && ! ("JSON" == n.type))
					n.favicon_url = µ.get_favicon_url(rep, n.domain_part)
				var fdgs = []
				if ("JSON" == n.type)
					fdgs = typeof(n.results) != "undefined"  ? $_("results", rep, n) : rep
				else
					try { fdgs = rep.querySelectorAll(n.results)}
					catch(e){console.error(i, rep, e)}
				var fdgs_nb = fdgs ? fdgs.length : 0
				var res_nb = 0
				if (n.extends != "RSS" && n.extends != "ATOM") {
					try { res_nb = $_("res_nb", rep, n)}
					catch(e) {console.log(i, "res_nb", rep, e)}
				}
				res_nb = Number(res_nb) || fdgs_nb
				var max_fdgs = MAX_RES_BY_SRC
				fdgs_nb = Math.min(fdgs_nb, max_fdgs)
				mp_req.metaFindingsList.add({
					mf_icon: n.favicon_url,
					mf_name: n.tags.name,
					mf_title: mf_title(n.tags.name),
					mf_res_nb: res_nb,
					mf_locale_res_nb: res_nb.toLocaleString(),
					// mf_loc_res_nb: fdgs_nb,
					// mf_locale_loc_res_nb: fdgs_nb.toLocaleString(),
				})
				if (!fdgs_nb) return search_query_countdown()
				// console.log(fdgs)
				var f_h1="", f_url="", f_txt="", f_by="", f_dt=new Date(0), f_nb=0
				for (let f of fdgs) {
					if (max_fdgs-- == 0) break
					f_nb = MAX_RES_BY_SRC - max_fdgs
					f_h1 = ""; f_url=""; f_txt=""; f_by=""; f_dt=new Date(0)
					try { f_h1	= µ.triw($_("r_h1",f,n)) }
					catch(exc) { console.error(i, f_nb, "f_h1", f, exc) }
					try { f_url = µ.urlify($_("r_url", f, n), n.domain_part) }
					catch (exc) { console.error(i, f_nb, "f_url", f, exc)}
					try { f_txt = µ.no_inline_style(µ.triw($_("r_txt", f, n)))}
					catch (exc) { console.error(i, f_nb, "f_txt", f, exc)}
					try { f_by	= µ.triw($_("r_by", f, n)) || n.tags.name }
					catch (exc) { console.error(i, f_nb, "f_by", f, exc)}
					try { f_dt	= parse_dt_str($_("r_dt", f, n), n.tags.tz, i) }
					catch (exc) { console.error(i, f_nb, "f_dt", f, exc) }
					try {
						mp_req.findingList.add({f_h1:f_h1, f_url:f_url, f_by:f_by,
							f_txt: µ.bolden(µ.shorten(f_txt, EXCERPT_SIZE), QUERY_STR),
							f_title_txt:f_txt && (f_txt.length > EXCERPT_SIZE) ?
								µ.HTML_decode_entities(f_txt) : "",
							f_title_by:	f_by,
							f_dt: f_dt.toISOString().slice(0, -14),
							f_epoc:	f_dt.valueOf(),
							f_ISO_dt: f_dt.toISOString(),
							f_title_dt: f_dt.toLocaleString(),
							f_icon:	n.favicon_url,
							f_source: n.tags.name,
						})
					} catch (exc) { console.error(i, f_nb, f_dt, exc) }
				}
				search_query_countdown()
				console.log(i, new Date() - src_query_start_date, "ms",
					mp_req.running_query_countdown, "/", current_source_nb, ":", res_nb, "res")
			})
		}
	})
	function search_query_countdown() {
		mp_req.running_query_countdown -= 1
		document.getElementById("waiting_anim").style.display = "none"
		// update_progressbar('findings_progressbar',
		// Math.round(100 - mp_req.running_query_countdown * 100 / current_source_nb));
		clearTimeout(mp_req.query_result_timeout)
		mp_req.query_result_timeout = setTimeout(() => { display_ongoing_results() }, 750)
	}
	function display_ongoing_results() {
		mp_req.findingList.sort("f_epoc", {order: "desc"})
		mp_req.metaFindingsList.show(0, mp_req.metaFindingsList.size())
		for (let a of document.getElementsByClassName("mf_name")) {
			a.addEventListener("click", evt => {
				mp_req.findingList.search(evt.target.textContent, "f_source")
				bold_clicked_a(evt.target, "mf_name")
				mf_date_slicing()
				filter_last(document.getElementById("mf_all_res"), null)
			})
		}
		mp_req.metaFindingsList.show(0, META_FINDINGS_PAGE_SIZE)
		mp_req.metaFindingsList.sort("mf_res_nb", {order: "desc"})
		document.getElementById("mf_total").addEventListener("click", evt => {
			mp_req.findingList.search()
			bold_clicked_a(evt.target, "mf_name")
			mf_date_slicing()
			filter_last(document.getElementById("mf_all_res"), null)
		})
		mf_date_slicing()
		for (let sbm of document.getElementsByClassName("sidebar-module")) {
			sbm.style.display="block"
		}
		document.getElementById("mp_query_countdown").textContent =
			mp_req.running_query_countdown
		document.getElementById("mp_running_src_fetched").textContent = current_source_nb
		document.getElementById("mp_running_stats").style.display = "block"
		if (! mp_req.final_result_displayed && mp_req.running_query_countdown == 0)
			display_final_results()
	}
	function display_final_results() {
		mp_req.final_result_displayed = 1
		location.href = "#"; location.href = "#before_search_stats"
		// reset_progressbar('findings_progressbar');
		document.getElementById("mp_running_stats").style.display = "none"
		document.getElementById("search_stats").style.display="block"
		clearTimeout(mp_req.duration_ticker)
		let mf_res_nb = 0
		for (let r of mp_req.metaFindingsList.items) mf_res_nb += r.values().mf_res_nb
		if (0 == mp_req.findingList.size()) {
			document.getElementById("no_results").style.display = "block"
		} else {
			document.getElementById("mp_page_sizes").style.display = "block"
		}
		document.getElementById("mp_query_meta_total").textContent = mf_res_nb.toLocaleString()
		document.getElementById("mp_query_meta_local_total").textContent =
			mp_req.findingList.size()
		document.getElementById("mp_duration").textContent =
			(ndt()-mp_req.query_start_date) / 1000
		document.getElementById("mp_query_search_terms").textContent = mp_req.search_terms
		document.getElementById("mp_src_nb").textContent =
			mp_req.metaFindingsList.items.filter(i => i.values().mf_res_nb > 0).length
		document.getElementById("mp_src_fetched").textContent = current_source_nb
		document.getElementById("mp_submit").disabled = false
		document.getElementById("mp_submit").style.cursor = "pointer"
		document.getElementById("mp_stop").style.display = "none"
	}
	function clear_results() {
		mp_req.final_result_displayed = 0
		document.getElementById("no_results").style.display = "none"
		document.getElementById("mp_page_sizes").style.display = "none"
		document.getElementById("mp_query_meta_total").textContent = "…"
		document.getElementById("mp_query_meta_local_total").textContent = "…"
		document.getElementById("mp_running_stats").style.display = "none"
		document.getElementById("mp_query_countdown").textContent = "…"
		document.getElementById("mp_running_src_fetched").textContent = "…"
		document.getElementById("mp_duration").textContent = "…"
		document.getElementById("mp_query_search_terms").textContent = "…"
		document.getElementById("mp_src_nb").textContent = "…"
		document.getElementById("mp_src_fetched").textContent = "…"
		document.getElementById("mp_duration").textContent = "…"
		mp_req.findingList.clear()
		mp_req.metaFindingsList.clear()
		document.getElementById("mp_clear_filter").click()
	}
	document.getElementById("mp_stop").addEventListener("click", () => {
		// cancel running promises -> impossible in 2019
		// clear_results()
		// location.reload()
		let new_url = new URL(window.location)
		new_url.searchParams.set('q', document.getElementById("mp_query").value)
		window.location = new_url.href.split('#')[0]
	})
	document.getElementById("mp_headlines_page_5").addEventListener("click", () => {
		HEADLINE_PAGE_SIZE=5; headlineList.show(0, HEADLINE_PAGE_SIZE)})
	document.getElementById("mp_headlines_page_10").addEventListener("click", () => {
		HEADLINE_PAGE_SIZE=10; headlineList.show(0, HEADLINE_PAGE_SIZE)})
	document.getElementById("mp_headlines_page_20").addEventListener("click", () => {
		HEADLINE_PAGE_SIZE=20; headlineList.show(0, HEADLINE_PAGE_SIZE)})
	document.getElementById("mp_headlines_page_50").addEventListener("click", () => {
		HEADLINE_PAGE_SIZE=50; headlineList.show(0, HEADLINE_PAGE_SIZE)})
	document.getElementById("mp_headlines_page_all").addEventListener("click", () => {
		HEADLINE_PAGE_SIZE=headlineList.size(); headlineList.show(0, HEADLINE_PAGE_SIZE)})
	document.getElementById("mp_page_10").addEventListener("click", () => {
		mp_req.findingList.show(0, 10)})
	document.getElementById("mp_page_20").addEventListener("click", () => {
		mp_req.findingList.show(0, 20)})
	document.getElementById("mp_page_50").addEventListener("click", () => {
		mp_req.findingList.show(0, 50)})
	document.getElementById("mp_page_all").addEventListener("click", () => {
		mp_req.findingList.show(0, mp_req.findingList.size())})
	document.getElementById("mp_import_JSON").addEventListener("click", () => {
		let input_elt = document.createElement("input")
		input_elt.type = "file"
		input_elt.click()
		input_elt.addEventListener("change", () => {
			mp_req.query_start_date = ndt()
			let file = input_elt.files[0]
			if (file) {
				let reader = new FileReader()
				reader.readAsText(file, "UTF-8")
				reader.onload = function (evt) {
					let input_json = JSON.parse(evt.target.result)
					tags_fromJSON(input_json.filters)
					clear_results()
					mp_req.findingList.add(input_json.findings)
					mp_req.metaFindingsList.add(input_json.meta_findings)
					display_ongoing_results()
					display_final_results()
				}
				reader.onerror = () => console.error("error importing JSON file")
			}
		}, false)
	})
	document.getElementById("mp_import_RSS").addEventListener("click", e=>import_XML(e, "RSS"))
	document.getElementById("mp_import_ATOM").addEventListener("click",e=>import_XML(e,"ATOM"))
	function import_XML(evt, flux_type) {
		let input_elt = document.createElement("input")
		input_elt.type = "file"
		input_elt.click()
		input_elt.addEventListener("change", () => {
			mp_req.query_start_date = ndt()
			let file = input_elt.files[0]
			if (file) {
				let reader = new FileReader()
				reader.readAsText(file, "UTF-8")
				reader.onload = function (evt) {
					let input_xml = µ.dom_parser.parseFromString(evt.target.result,
						"application/xml")
					clear_results()
					var r_source, n, r_dt, r_by, f_source, f_icon, item_obj, f_txt
					var mf = {}
					var p = provided_sources[flux_type] // parser info
					tags_fromXML(input_xml.querySelectorAll(p.filters), flux_type)
					for (let item of input_xml.querySelectorAll(p.results)) {
						r_source = $_("r_src", item, p)
						n = provided_sources[r_source]
						// if (!n.domain_part) n.domain_part = µ.domain_part(n.headline_url);
						// if (!n.favicon_url) n.favicon_url =
						//	µ.get_favicon_url(rep, n.domain_part);
						r_dt = new Date($_("r_dt", item, p))
						r_by = $_("r_by", item, p)
						f_source = n && n.tags.name || µ.domain_name(r_source) || r_source || ""
						f_icon = $_("favicon_url", item, p) || (n && n.favicon_url)
							|| "/img/Feed_icon_48.png"
						f_txt = µ.bolden(
							µ.shorten(
								µ.no_inline_style(
									µ.triw(
										$_("r_txt", item, p)))))
						item_obj = {
							f_h1: $_("r_h1", item, p),
							f_url: $_("r_url", item, p),
							f_icon: f_icon,
							f_txt: f_txt,
							f_by: r_by,
							f_title_by: r_by,
							f_dt: r_dt.toISOString().slice(0, -14),
							f_epoc: r_dt.valueOf(),
							f_ISO_dt: r_dt.toISOString(),
							f_title_dt: r_dt.toLocaleString(),
							f_source: f_source
						}
						mp_req.findingList.add(item_obj)
						if (typeof(mf[r_source]) == "undefined") {
							mf[r_source] = {
								mf_icon: f_icon,
								mf_title: mf_title(f_source),
								mf_name: item_obj.f_source,
								mf_res_nb: 0
							}
						}
						mf[r_source].mf_res_nb += 1
						mf[r_source].mf_locale_res_nb = mf[r_source].mf_res_nb.toLocaleString()
					}
					var v
					for (let k of Object.keys(mf)) {
						v = mf[k]
						mp_req.metaFindingsList.add(v)
					}
					display_ongoing_results()
					display_final_results()
				}
				reader.onerror = () => {console.error(`error importing ${flux_type} file`)}
			}
		}, false)
	}
	function mf_title(src_name) {return `Filter results of "${src_name}" only`}
	/*
	document.getElementById("mp_export_search_JSON").addEventListener("click", () => {
		let json_str=`{
			"filters": ${JSON.stringify(tags_toJSON())},
			"findings": ${JSON.stringify(mp_req.findingList.toJSON())},
			"meta_findings": ${JSON.stringify(mp_req.metaFindingsList.toJSON())}
		}`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.json`, json_str)
	})*/
	document.getElementById("mp_sel_mod").addEventListener("click", () => {
		let first_checkbox = document.getElementsByClassName("f_selection")[0]
		let cur_sel_mod = first_checkbox.style.display
		if (cur_sel_mod == "") cur_sel_mod = "none"
		let next_sel_mod = cur_sel_mod == "none" ? "block" : "none"
		// let next_sel_mod = cur_sel_mod == "none" ? "inline-grid" : "none"
		document.getElementsByClassName("mp_export_choices")[0].style.display=next_sel_mod
		let cur_page_size = mp_req.findingList.page
		mp_req.findingList.show(0, mp_req.findingList.size())
		for (let li of document.querySelectorAll("#findings ul.list li")){
			li.getElementsByClassName("f_selection")[0].style.display=next_sel_mod
			if (next_sel_mod == "none") {
				li.getElementsByClassName("f_url")[0].classList.remove("f_sel")
			} else {
				li.getElementsByClassName("f_url")[0].classList.add("f_sel")
			}
		}
		mp_req.findingList.show(0, cur_page_size)
	})
	document.getElementById("mp_export_sel_JSON").addEventListener("click", () => {
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName("f_selection")[0].checked})
		let cur_sel_items = []
		for (let i of mp_req.findingList.matchingItems) cur_sel_items.push(i.values())
		mp_req.findingList.filter()
		let json_str = `{
			"filters": ${JSON.stringify(tags_toJSON())},
			"findings": ${JSON.stringify(cur_sel_items)},
			"meta_findings": ${JSON.stringify(mp_req.metaFindingsList.toJSON())}
		}`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.json`, json_str)
	})
	document.getElementById("mp_export_sel_RSS").addEventListener("click", () => {
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName("f_selection")[0].checked})
		export_RSS()
		mp_req.findingList.filter()
	})
	document.getElementById("mp_export_search_RSS").addEventListener("click", () => {
		export_RSS()
	})
	function export_RSS() {
		// https://validator.w3.org/feed/#validate_by_input
		let flux_items = "", v = {}
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			flux_items += `<item>
				${title_line(v)}
				<description>${µ.encode_XML(v.f_txt)}</description>
				<link>${µ.encode_XML(v.f_url)}</link>
				<pubDate>${new Date(v.f_ISO_dt).toUTCString()}</pubDate>
				<dc:creator>${µ.encode_XML(v.f_by)}</dc:creator>
				<guid>${µ.encode_XML(v.f_url)}</guid>
			</item>`
		}
		let flux = `<?xml version="1.0" encoding="UTF-8"?>
		<rss version="2.0"
			xmlns:dc="http://purl.org/dc/elements/1.1/"
		>
			<channel>
				<title>Meta-Press.es</title>
				<description>Selection RSS 2.0</description>
				<link>https://www.meta-press.es</link>
				<lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
				<language>${tag_selectrs.lang.getValue() || userLang}</language>
				<generator>https://www.meta-press.es</generator>
				<docs>http://www.rssboard.org/rss-specification</docs>
				${tags_toXML("RSS")}
				${flux_items}
			</channel>
		</rss>`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.rss`, flux)
	}
	document.getElementById("mp_export_sel_ATOM").addEventListener("click", () => {
		// https://tools.ietf.org/html/rfc4287
		// https://validator.w3.org/feed/#validate_by_input
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName("f_selection")[0].checked})
		let flux_items = "", v = {}
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			flux_items += `<entry>
				${title_line(v)}
				<summary type="html">${µ.encode_XML(v.f_txt)}</summary>
				<published>${v.f_ISO_dt}</published>
				<updated>${v.f_ISO_dt}</updated>
				<link href="${µ.encode_XML(v.f_url)}"/>
				<author><name>${v.f_by}</name></author>
				<id>${µ.encode_XML(v.f_url)}</id>
			</entry>`
		}
		mp_req.findingList.filter()
		let flux = `<?xml version="1.0" encoding="UTF-8"?>
		<feed xmlns="http://www.w3.org/2005/Atom">
			<title>Meta-Press.es</title>
			<subtitle>Decentralize press search engine</subtitle>
			<link href="https://www.meta-press.es"/>
			<updated>${ndt().toISOString()}</updated>
			<author><name>Meta-Press.es</name></author>
			<id>https://www.meta-press.es/</id>
			${tags_toXML("ATOM")}
			${flux_items}
		</feed>`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.atom`, flux)
	})
	function title_line(v) { // v stands for values of the current result to serialize in XML
		const title_brackets = `[${v.f_source}]`
		if (v.f_h1.indexOf(title_brackets) != -1) {
			return `<title>[${v.f_source}] ${v.f_h1}</title>`
		} else {
			return `<title>${v.f_h1}</title>`
		}
	}
	function ndt() { return new Date() }
	var now = ndt()
	var date_filters = [
		function mf_last_day(r)  {return r.values().f_epoc>ndt().setDate(now.getDate() - 1)},
		function mf_last_2d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 2)},
		function mf_last_3d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 3)},
		function mf_last_week(r) {return r.values().f_epoc>ndt().setDate(now.getDate() - 7)},
		function mf_last_2w(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 14)},
		function mf_last_month(r){return r.values().f_epoc>ndt().setMonth(now.getMonth() - 1)},
		function mf_last_2m(r)	 {return r.values().f_epoc>ndt().setMonth(now.getMonth() - 2)},
		function mf_last_6m(r)	 {return r.values().f_epoc>ndt().setMonth(now.getMonth() - 6)},
		function mf_last_1y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-1)},
		function mf_last_2y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-2)},
		function mf_last_5y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-5)},
		function mf_last_dk(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-10)}
	]
	for (let flt of date_filters) document.getElementById(flt.name).addEventListener("click",
		evt => filter_last(evt.target.parentNode.parentNode, flt))
	document.getElementById("mf_all_res").addEventListener("click",
		evt => filter_last(evt.target.parentNode.parentNode, null))
	function mf_date_slicing() {
		var prev_nb = 0
		var total_nb = set_filter_nb(null, "mf_all_res", 0, 0)
		for (let flt of date_filters) {
			prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb)
		}
	}
	function set_filter_nb(fct, id, prev_nb, total_nb) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		var nb = mp_req.findingList.matchingItems.length
		if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
			let p = Math.floor(nb * 40 / mp_req.findingList.size())
			document.getElementById(id+"_nb").textContent =
				`${nb.toLocaleString()} ${".".repeat(p)}`
			document.getElementById(id).style.display="block"
		} else {
			document.getElementById(id).style.display="none"
		}
		return nb
	}
	function filter_last(target, fct) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		bold_clicked_a(target, "mf_dt_filter")
	}
	function bold_clicked_a(target, a_class) {
		for (let i of document.getElementsByClassName(a_class)) { i.classList.remove("bold") }
		target.classList.add("bold")
	}
	document.getElementById("mp_filter").addEventListener("keyup", searchInResCallback)
	document.getElementById("mp_clear_filter").addEventListener("click", () => {
		document.getElementById("mp_filter").value = ""
		searchInResCallback({target: {value: ""}})
	})
	var searchInResCallback_timeout
	function searchInResCallback (evt) {
		clearTimeout(searchInResCallback_timeout)
		searchInResCallback_timeout = setTimeout(() => {
			if (mp_req.findingList.searched) { document.getElementById("mf_total").click() }
			if (mp_req.findingList.filtered) {filter_last(document.getElementById("mf_all_res"),
				null) }
			if (evt.target.value) {
				mp_req.findingList.search(evt.target.value, ["f_h1", "f_source", "f_dt", "f_by",
					"f_txt"])
			} else {
				mp_req.findingList.search()
			}
			mf_date_slicing()
		}, evt.keyCode == 13 ? 0 : 500)
	}
	function parse_dt_str(dt_str, tz) {
		var d = undefined
		if (/\d{12,20}/i.test(dt_str)) {
			d = new Date(Number(dt_str))
		} else if (/min(utes?)?( ago)?/i.test(dt_str)) {
			d = µ.timezoned_date("", tz)
			d.setMinutes(d.getMinutes() - Number(dt_str.replace(/min(utes?)? ago/, "")))
		} else if (/hours?( ago)?/i.test(dt_str)) {
			d = µ.timezoned_date("", tz)
			d.setHours(d.getHours() - Number(dt_str.replace(/hours? ago/, "")))
		} else if (/il y a \d+h/i.test(dt_str)) {
			d = µ.timezoned_date("", tz)
			d.setHours(d.getHours() - Number(dt_str.replace(/(\d+)/, "$1")))
		} else if (/(today|new)/i.test(dt_str)) {
			d = µ.timezoned_date("", tz)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/yesterday|hier/i.test(dt_str)) {
			d = µ.timezoned_date("", tz)
			d.setDate(d.getDate() - 1)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (µ.HH_MM_RE.test(dt_str)) {
			dt_str = dt_str.replace(/{(.*)}/, (_, $1) => month_nb($1, month_nb_json))
			try {
				d = µ.timezoned_date(dt_str, tz)
			} catch (exc) {
				console.log(`'${dt_str}' failed to be converted to new Date() with hours`)
				d = µ.timezoned_date(0, tz)
			}
			d.setMinutes(0)
			d.setSeconds(0)
		} else {
			if (typeof(dt_str) == "string")
				dt_str = dt_str.replace(/{(.*)}/, (_, $1) => month_nb($1, month_nb_json))
			d = µ.timezoned_date(dt_str, tz)
		}
		if (!d.getMinutes() && !d.getSeconds()) {  // no time set ? put random one
			if (µ.HH_MM_RE.test(dt_str)) {
				let d_split = µ.regextract(µ.HH_MM_STR, dt_str).split(':')
				d.setHours(Number(d_split[0]))
				d.setMinutes(Number(d_split[1]))
			} else {
				d.setHours(Math.min(ndt().getHours(),
					µ.rnd(1)+µ.rnd(1)+(Math.floor((µ.rnd(1)+1)/2))))
			}
		}
		if (isNaN(d)) throw new Error(`'${dt_str}' (${tz}) is an invalid new Date()`)
		return d
	}
	function format_search_url(search_url, token) {
		return search_url.replace("{}", token)
			.replace("{#}", MAX_RES_BY_SRC)
			.replace('{+}', token.replace(' ', '+'))
		//		.replace('{%}', encodeURIComponent(token))
	}
	function $_(elt, dom, src) {  // parsing lookup
		var val = ""
		if (src.type && src.type == "JSON") {
			let src_elt = src[elt]
			if (typeof(src_elt) == 'undefined') return ""
			try {
				val = dom
				for (let json_path_elt of src[elt].split(".")) val = val[json_path_elt]
				if (elt != "results" &&
						typeof(val) == 'object' && typeof(val.length) != 'undefined') { // Array
					let val_acc = ''
					let val_attr = src[`${elt}_attr`]
					for (let v of val) {
						if (typeof(val_attr) == 'string')
							val_acc += `${v[val_attr]}, `
						else
							val_acc += v
					}
					val = val_acc
				}
			} catch (exc) { console.error(src.tags.name, elt, exc, dom) }
		} else {
			let src_elt = src[elt]
			if (typeof(src_elt) == "string") {
				val = dom.querySelector(src_elt)
			} else {
				let elt_xpath = src[`${elt}_xpath`]
				if (typeof(elt_xpath) == "string") {
					val = µ.evaluateXPath(dom, elt_xpath)
				} else { // console.error(
					// `Missing ${elt} (or ${elt}_xpath) in ${src.tags.name}, or not string.`)
					return ""
				}
			}
			var src_elt_attr = src[`${elt}_attr`]
			if (typeof (src_elt_attr) == "string") {
				try {
					val = val.getAttribute(src_elt_attr)
				} catch (exc) { console.error(src.tags.name, src_elt_attr, exc, dom)
					val = ""
				}
			} else {
				try {
					val = µ.triw(val.textContent)
				} catch (exc) {
					console.error(src.tags.name, elt, exc, dom)
					if (elt == 'r_dt') {
						val = "100000000000"  // an old date Sat Mar 03 1973 09:46:40 GMT+0000
					} else {
						val = ""
					}
				}
			}
		}
		// if (elt == 'r_dt') console.log('before', val)
		var elt_re = src[`${elt}_re`]  // it should be a list of 2 elements
		if (typeof (elt_re) == "object" && typeof (val) == "string") {
			val = µ.regextract(elt_re[0], val, elt_re[1])
		}
		// if (elt == 'r_dt') console.log('after', val)
		return val
	}
	/* * * Headlines * * */
	var headlineList = new List("headlines", {
		item: "headline-item",
		valueNames: [
			"h_title",
			{ name: "h_url",	attr: "href" },
			{ name: "h_icon",	attr: "src" },
			{ name: "h_tip",	attr: "title" },
		],
		page: HEADLINE_PAGE_SIZE,
		pagination: {
			outerWindow: 10,
			innerWindow: 10
		}
	})
	var headline_item = 1
	var headline_timeout, headline_fadeout_timeout
	function rotate_headlines_timeout() {
		headline_timeout = setTimeout(() => {
			if (headlineList.size() > HEADLINE_PAGE_SIZE) {
				headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size()
				headlineList.show(headline_item, HEADLINE_PAGE_SIZE)
				rotate_headlines()
			}
		}, 10000)
	}
	function rotate_headlines() {
		clearTimeout(headline_fadeout_timeout)
		rotate_headlines_timeout()
		fadeout_pagination_dot("#headlines", "rgba(0, 208, 192, 1)", 1)
	}
	function fadeout_pagination_dot(id, bg_color, dot_opacity) {
		headline_fadeout_timeout = setTimeout(() => {
			var dot = document.querySelector(`${id} ul.pagination li.active a.page`)
			dot_opacity -= 0.1
			dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`)
			fadeout_pagination_dot(id, bg_color, dot_opacity)
		}, 1000)
	}
	document.getElementById("headlines").addEventListener("mouseover", () => {
		clearTimeout(headline_timeout)
		clearTimeout(headline_fadeout_timeout)
	})
	document.getElementById("headlines").addEventListener("mouseout", () => {
		rotate_headlines()
	})
	function load_headlines() {
		headlineList.clear()
		let i = 0
		let sub_load_headline = async (k) => {
			let n = sources_objs[k]
			if (! n.headline_url) { return }
			i += 1
			µ.sleep(1000 * i)
			let rep = await fetch(n.headline_url)
			if (!rep.ok) throw `status ${rep.status}`
			let c_type = rep.headers.get("content-type")
			let r = µ.dom_parser.parseFromString(await rep.text(), µ.clean_c_type(c_type))
			try {
				if (!n.domain_part) n.domain_part = µ.domain_part(n.headline_url)
				if (!n.favicon_url) n.favicon_url = µ.get_favicon_url(r, n.domain_part)
				var h = r.querySelector(n.headline_selector)
				headlineList.add({
					h_title:µ.shorten(µ.triw(h.textContent), HEADLINE_TITLE_SIZE),
					h_url:	µ.urlify(h.getAttribute("href"), n.domain_part),
					h_icon: n.favicon_url,
					h_tip: `Headline from "${n.tags.name}" (${ndt().toLocaleString()})\n`+
						`${µ.triw(h.textContent)}`
				})
			} catch (err) {console.error("header for", k, err) }
		}
		for (let k of current_source_selection.slice(0, MAX_HEADLINE_LOADING)) {
			sub_load_headline(k)
		}
	}
	/* * * Tags * * */
	var selectr_opt = {	clearable: false, placeholder: "*", mobileDevice: false}
	// already tried nativeDropdown: false // but was unsuccessfull
	var tag_selectrs = {
		"tech": new Selectr("#tags_tech", Object.assign({}, selectr_opt)),
		"lang": new Selectr("#tags_lang", Object.assign({}, selectr_opt)),
		"country": new Selectr("#tags_country", Object.assign({}, selectr_opt)),
		"themes": new Selectr("#tags_themes", Object.assign({}, selectr_opt)),
		"src_type": new Selectr("#tags_src_type", Object.assign({}, selectr_opt)),
		"res_type": new Selectr("#tags_res_type", Object.assign({}, selectr_opt)),
		"scope": new Selectr("#tags_scope", Object.assign({}, selectr_opt)),
		"freq": new Selectr("#tags_freq", Object.assign({}, selectr_opt)),
		"name": new Selectr("#tags_name", Object.assign({}, selectr_opt)),
	}
	function populate_tags(source_selection) {
		var available_tags = {}
		for (let tag_name of Object.keys(tag_selectrs)) {
			available_tags[tag_name] = {}
		}
		for (let k of source_selection) {
			let n = sources_objs[k]
			if (typeof(n) == 'undefined') return
			for (let tag_name of Object.keys(tag_selectrs)) {
				let tag_val = n.tags[tag_name]
				if (typeof(tag_val) == "string") {
					available_tags[tag_name][tag_val] = 1+available_tags[tag_name][tag_val] || 1
				} else if (typeof(tag_val) == "object") {
					for (let tag_val_itm of tag_val) {
						available_tags[tag_name][tag_val_itm] =
							1 + available_tags[tag_name][tag_val_itm] || 1
					}
				} else {
					console.error(`Unknown tag type ${tag_name} ${tag_val} for ${k}`)
				}
			}
		}
		for (let tag_name of Object.keys(tag_selectrs)) {
			let s = tag_selectrs[tag_name]
			let s_value = s.getValue()
			s.off("selectr.change", on_tag_sel)
			s.close()
			s.clear()
			s.removeAll()
			for (var t of Object.keys(available_tags[tag_name]).sort())
				s.add({value: t, text:`${t}<small> (${available_tags[tag_name][t]})</small>`})
			s.setValue(s_value)
			s.on("selectr.change", on_tag_sel)
		}
		for (let dns_prefetch_link of document.getElementsByClassName(".mp_dns_prefetch")) {
			dns_prefetch_link.outerHTML = ""
		}
		for (let i of current_source_selection) {
			let n = sources_objs[i]
			if (typeof(n) == 'undefined') return
			let l = document.createElement("link")
			l.rel="dns-prefetch"
			l.href=n.search_url
			l.classList.add("mp_dns_prefetch")
			document.head.appendChild(l)
		}
	}
	function on_tag_sel(opt) {
		let new_src_sel = {}
		for (let tag_name of Object.keys(tag_selectrs)) {
			new_src_sel[tag_name] = {}
			for (let selected_tag of tag_selectrs[tag_name].getValue()) {
				if (tag_name != "tech") { // inner accumulation
					for (let k of sources_keys)
						if (sources_objs[k].tags[tag_name].includes(selected_tag))
							new_src_sel[tag_name][k] = ""
				} else { // inner intersection
					let cur_tag_keys = []
					for (let k of sources_keys)
						if (sources_objs[k].tags[tag_name].includes(selected_tag))
							cur_tag_keys.push(k)
					if (Object.keys(new_src_sel["tech"]).length == 0) {
						for (let c of cur_tag_keys)
							new_src_sel["tech"][c] = ""
						continue
					}
					let prec_tag_keys = Object.keys(new_src_sel[tag_name])
					for (let k of prec_tag_keys.filter(e => cur_tag_keys.includes(e))) {
						new_src_sel[tag_name][k] = ""
					}
				}
			}
			if (tag_selectrs[tag_name].getValue().length > 0 &&
				Object.keys(new_src_sel[tag_name]).length == 0) {
				empty_sel(opt)
				return
			}
		}
		var new_src_sel_keys = sources_keys
		var tag_sel_keys = {}
		for (let tag_name of Object.keys(tag_selectrs)) { // get the smallest ensemble
			if (new_src_sel_keys.length == 0) {
				break
			} else {
				tag_sel_keys = Object.keys(new_src_sel[tag_name])
				if (tag_sel_keys.length > 0)
					new_src_sel_keys = new_src_sel_keys.filter(
						v => tag_sel_keys.includes(v))
			}
		}
		if (new_src_sel_keys.length > 0) {
			current_source_selection = new_src_sel_keys
			current_source_nb = current_source_selection.length
			browser.storage.sync.set({filters: tags_toJSON()})
			load_headlines()
		} else {
			empty_sel(opt)
		}
		document.getElementById("cur_tag_sel_nb").textContent = current_source_nb
	}
	function empty_sel(opt) {
		alert('This choice would result in an empty selection.')
		if (opt && typeof (opt.parentNode) != "undefined") {
			tag_selectrs[opt.parentNode.id.replace('tags_', '')].setValue(opt.value)
		} else {
			set_default_tags()
		}
		on_tag_sel({})
	}
	function tags_toJSON() {
		let tags = {}
		for (let t of Object.keys(tag_selectrs)) tags[t] = tag_selectrs[t].getValue()
		return tags
	}
	function tags_toXML(type) {
		let tags = "", tag_val
		for (let t of Object.keys(tag_selectrs)) {
			tag_val = tag_selectrs[t].getValue()
			if (typeof(tag_val) == "string") {
				if ("ATOM" == type) tags += `<category term="${tag_val}" label="mp__${t}" />\n`
				else tags += `<category>mp__${t}__${tag_val}</category>\n`
			} else if (typeof(tag_val) == "object")
				for (let v of tag_val)
					if ("ATOM" == type) tags += `<category term="${v}" label="mp__${t}" />\n`
					else tags += `<category>mp__${t}__${v}</category>\n`
		}
		return tags
	}
	function tags_fromXML(tag_nodes, xml_type) {
		let tag_values = {}
		for (let t of Object.keys(tag_selectrs)) tag_values[t] = []
		for (let c of tag_nodes)
			if ("ATOM" == xml_type) {
				let c_name = c.getAttribute("label")
				try {
					tag_values[c_name.split("__")[1]].push(`${c.getAttribute("term")}`)
				} catch (exc) { console.error(`Bad XML filter name "${c_name}" (${exc}).`) }
			} else {
				let c_split = c.textContent.split("__")
				try {
					tag_values[c_split[1]].push(`${c_split[2]}`)
				} catch (exc) {console.error(`Bad filter name "${c.textContent}" (${exc}).`) }
			}
		tags_fromJSON(tag_values)
		on_tag_sel({})
	}
	function tags_fromJSON(tag_values) {
		let s, v
		for (let tag_name of Object.keys(tag_values)) {
			s = tag_selectrs[tag_name]
			v = tag_values[tag_name]
			s.off("selectr.change", on_tag_sel)
			s.clear()
			s.setValue(v)
			s.on("selectr.change", on_tag_sel)
		}
		on_tag_sel({})
	}
	function is_virgin_tags() {
		var res = true
		for (let k of Object.keys(tag_selectrs)) {
			let v = tag_selectrs[k]
			if (v.getValue().length > 0) {
				res = false
			}
		}
		return res
	}
	function is_2nd_row_tags() {
		var res = false
		for (let k of ['res_type', 'scope', 'freq', 'name']) {
			let v = tag_selectrs[k]
			if (v.getValue().length > 0) {
				res = true
			}
		}
		return res
	}
	document.getElementById('reset_tag_sel').addEventListener('click', () => {
		set_default_tags()
	})
	function set_default_tags() {
		console.info("Meta-Press.es : loading default tags")
		tags_fromJSON({
			'tech': [],
			'lang': [],
			'themes': [],
			'country': [],
			'src_type': [],
			'res_type': [],
			'scope': [],
			'freq': [],
			'name': []
		})
		tags_fromJSON({
			"tech": ["HTTPS", "fast", "AND"],
			"lang": [userLang.slice(0, 2).toLowerCase()]
		})
	}
	function load_stored_tags(stored_tags) {
		populate_tags(current_source_selection)
		if (typeof(stored_tags) == "object" && typeof(stored_tags.filters) == "object") {
			tags_fromJSON(stored_tags.filters)
		}
		if (is_virgin_tags())
			set_default_tags()
		if (is_2nd_row_tags())
			document.getElementById('tags_2nd_row_handle').click()
		var dt_loaded_sources = new Date()
		document.getElementById('mp_loaded_source_total').textContent = sources_keys.length - 2
		document.getElementById('mp_loaded_source_duration').textContent =
			(dt_loaded_sources - dt_loading_sources) / 1000
		document.getElementById('mp_loaded_source_countries').textContent =
			tag_selectrs.country.data.length
		document.getElementById('mp_loaded_source_langs').textContent =
			tag_selectrs.lang.data.length
		rotate_headlines_timeout()
	}
	/* * * end tags * * */
	function update_current_sources(sources_objs) {
		sources_keys = Object.keys(sources_objs)
		current_source_selection = sources_keys
		current_source_nb = sources_keys.length
	}
	function load_source_objs(stored_data) {
		if (typeof(stored_data) == "object" && typeof(stored_data.custom_src) == "string") {
			try {
				let custom_src_obj = JSON.parse(stored_data.custom_src)
				sources_objs = Object.assign(provided_sources, custom_src_obj)
			} catch (exc) { alert(`Error parsing user custom source (${exc}).`) }
		} else {
			sources_objs = provided_sources
		}
		update_current_sources(sources_objs)
		/* * * current_source_selection defined * * */
		for (let k of sources_keys) {
			if (!k.startsWith("http")) continue
			let n = sources_objs[k]
			if (typeof(n.extends) != "undefined") { // Manage sources to extend
				let extended_src = sources_objs[n.extends]
				if (typeof(extended_src) != "undefined") {
					extended_src = JSON.parse(JSON.stringify(extended_src))
					let extended_tags = Object.assign(extended_src.tags || {}, n.tags)
					Object.assign(extended_src, n)
					n = extended_src
					n.tags = extended_tags
					sources_objs[k] = n
				}
			}
			if (n.tags.tech.includes("broken")) {
				delete sources_objs[k]
				update_current_sources(sources_objs)
				continue
			}
			if (n.search_url.startsWith("https")) { // Dynamically add the https/http src tag
				if (!n.tags.tech.includes("HTTPS"))
					n.tags.tech.push("HTTPS")
			} else {
				if (!n.tags.tech.includes("HTTP"))
					n.tags.tech.push("HTTP")
			}
		}
		browser.storage.sync.get("filters").then(
			load_stored_tags, err => {console.error(`Failed to retrieve filters: ${err}`)}
		)
	}
	function set_query_placeholder() {
		var val = [
			"La Quadrature du Net",
			"ADPRIP",
			"Grimoire-Command.es",
			"Le Petit Prince",
			"Hong Kong",
			"Yellow vests",
			"\"Nuit Debout\"",
			"Alexandre Benalla",
			"Delta-Chat",
			"F-Droid.org",
			"OpenContacts",
			"AsciiDoctor",
			"Internet ou Minitel 2.0",
			"Pablo Servigne",
			"Laurent Gaudé",
			"Le Prince de Machiavel",
			"Pensées pour moi-même",
			"La princesse de Clèves",
			"Alpine Linux",
			"Fairphone",
			"Tor Browser",
			"Wikipedia",
			"OpenStreetMap",
			"GridCoin",
			"Pinebook Pro",
			"ReasonML",
			"Leto Atreides",
			"Pliocene Exile",
			"Another World by Éric Chahi",
			"5 centimenters per second",
			"Nausicaä",
			"Repair Café",
			"Reporterre",
			"Chelsea Manning",
		]
		document.getElementById('mp_query').placeholder =
			`Search terms, ex : ${val[µ.pick_between(0, val.length)]}`
	}
	document.getElementById('tags_2nd_row_handle').addEventListener('click', () => {
		document.getElementById('tags_2nd_row').style.display = document.getElementById(
			'tags_2nd_row').style.display == 'none' ? 'block' : 'none'
		document.getElementById('tags_2nd_row_handle').textContent = document.getElementById(
			'tags_2nd_row_handle').textContent == '[+]' ? '[-]' : '[+]'
	})
	/* * end definitions * */
	set_query_placeholder()
	let month_nb_json = await fetch("js/month_nb/month_nb.json")
	month_nb_json = await month_nb_json.json()
	var dt_loading_sources = new Date()
	var provided_sources = await fetch("json/sources.json")
	provided_sources = await provided_sources.json()
	var custom_stored_source = await browser.storage.sync.get("custom_src")/*.then(
		load_source_objs, err => {console.error(`Failed to retrieve custom_src: ${err}`)}
	)*/
	var manifest = await fetch("manifest.json")
	manifest = await manifest.json()
	document.getElementById('mp_version').textContent = 'v' + manifest.version
	load_source_objs(custom_stored_source)
})()
