zopfli_fork:
	rsync -av \
			--exclude *.git* \
			--exclude *.swp \
			--exclude *LICENSE* \
			--exclude Makefile \
			--exclude README.adoc \
			--exclude TODO.adoc \
			--exclude font \
			--exclude js/month_nb/READ* \
			--exclude js/month_nb/LIC* \
			--exclude js/month_nb/.git \
			--exclude js/month_nb/month_definitions \
			--exclude js/month_nb/reimplementations \
			--exclude js/month_nb/load_month_nb.js \
			--exclude js/month_nb/test_month_nb.mjs \
			--exclude js/month_nb/month_nb_node-10.x.js \
			--exclude js/codemirror.js \
			--exclude js/codemirror_mode_javascript.js \
			--exclude css/bootstrap/js \
			--exclude css/bootstrap/css/bootstrap.css \
			--exclude css/bootstrap/css/bootstrap-theme.css \
			--exclude css/bootstrap/css/bootstrap-theme.min.css \
			--exclude css/bootstrap/config.json \
			--exclude img/src \
		. /tmp/`date +%F_%X`-meta-press-ext_zopfli/
	cd /tmp && zopfli_fork --t$(`nproc` / 2) --zip --dir `date +%F_%X`-meta-press-ext_zopfli
	mv /tmp/*.zip ../

7zip:
	7z a -tzip -mx=9 \
			-x!.git \
			-x!.gitmodules \
			-x!*.swp \
			-x!*LICENSE* \
			-x!Makefile \
			-x!README.adoc \
			-x!TODO.adoc \
			-x!font \
			-x!js/month_nb/READ* \
			-x!js/month_nb/LIC* \
			-x!js/month_nb/.git \
			-x!js/month_nb/month_definitions/* \
			-x!js/month_nb/reimplementations/* \
			-x!js/month_nb/load_month_nb.js \
			-x!js/month_nb/test_month_nb.mjs \
			-x!js/month_nb/month_nb_node-10.x.js \
			-x!js/codemirror.js \
			-x!js/codemirror_mode_javascript.js \
			-x!css/bootstrap/js/* \
			-x!css/bootstrap/css/bootstrap.css \
			-x!css/bootstrap/css/bootstrap-theme.css \
			-x!css/bootstrap/css/bootstrap-theme.min.css \
			-x!css/bootstrap/config.json \
			-x!img/src \
		../`date +%F_%X`-meta-press-ext_zip_7zip .

zip:
	zip -9 \
			-x *.git* \
			-x *.swp \
			-x *LICENSE* \
			-x Makefile \
			-x README.adoc \
			-x TODO.adoc \
			-x font/* \
			-x js/month_nb/READ* \
			-x js/month_nb/LIC* \
			-x js/month_nb/.git \
			-x js/month_nb/month_definitions/* \
			-x js/month_nb/reimplementations/* \
			-x js/month_nb/load_month_nb.js \
			-x js/month_nb/test_month_nb.mjs \
			-x js/month_nb/month_nb_node-10.x.js \
			-x js/codemirror.js \
			-x js/codemirror_mode_javascript.js \
			-x css/bootstrap/js/* \
			-x css/bootstrap/css/bootstrap.css \
			-x css/bootstrap/css/bootstrap-theme.css \
			-x css/bootstrap/css/bootstrap-theme.min.css \
			-x css/bootstrap/config.json \
			-x img/src \
		-v -r ../`date +%F_%X`-meta-press-ext_zip .

jsuglify:
	uglifyjs file.js > file.min.js

js-beautify:
	 js-beautify file.min.js > file.js

yui-compressor:
	yui-compressor -o file.min.js file.js

dl-compressd-dayjs-locales:
	for a in `ls /js/dayjs/locale/*.js`; do wget -O "js/dayjs/locale/`basename ${a} .js`.min.js" "https://unpkg.com/dayjs@1.8.17/locale/${a}"; done;


.PHONY: zip
